﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MyGame
{
    class Star: BaseObject
    {
        public Star(Point pos, Point dir, Size size): base(pos, dir, size)
        {
        }

        public override void Draw()
        {
            SplashScreen.Buffer.Graphics.DrawLine(Pens.White, Pos.X, Pos.Y, Pos.X + Size.Width, Pos.Y + Size.Height);
            SplashScreen.Buffer.Graphics.DrawLine(Pens.White, Pos.X + Size.Width, Pos.Y, Pos.X, Pos.Y + Size.Height);
        }
        public override void Update()
        {
            Pos.X = Pos.X - Dir.X;
            Pos.Y = Pos.Y + Dir.Y;
            if (Pos.X >= SplashScreen.Width-40) { Dir.X = -Dir.X; }
            if (Pos.X <= 1) { Dir.X = -Dir.X; }

            if (Pos.Y >= SplashScreen.Height-40) { Dir.Y = -Dir.Y; }
            if (Pos.Y <= 1) { Dir.Y = -Dir.Y; }
        }
    }
}
