﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyGame
{
    class SplashScreen
    {
        static Random r;

        private static BufferedGraphicsContext _context;
        public static BufferedGraphics Buffer;
        public static int Width { get; set; }
        public static int Height { get; set; }

        static SplashScreen()
        {
            r = new Random();
        }

        public static void Init(Form form)
        {
            // Графическое устройство для вывода графики
            Graphics g;
            // предоставляет доступ к главному буферу графического контекста для текущего приложения
            _context = BufferedGraphicsManager.Current;
            g = form.CreateGraphics(); // Создаём объект - поверхность рисования и связываем его с формой
                                       // Запоминаем размеры формы
            Width = form.Width;
            Height = form.Height;
            // Связываем буфер в памяти с графическим объектом.
            // для того, чтобы рисовать в буфере
            Buffer = _context.Allocate(g, new Rectangle(0, 0, Width, Height));

            Load();

            Timer timer = new Timer { Interval = 100 };
            timer.Start();

            timer.Tick += Timer_Tick;
        }

        private static void Timer_Tick(object sender, EventArgs e)
        {
            Draw();
            Update();
        }

        public static void Draw()
        {
            // Проверяем вывод графики
            //Buffer.Graphics.Clear(Color.Black);
            //Buffer.Graphics.DrawRectangle(Pens.White, new Rectangle(100, 100, 200, 200));
            //Buffer.Graphics.FillEllipse(Brushes.Wheat, new Rectangle(100, 100, 200, 200));
            //Buffer.Render();

            Buffer.Graphics.Clear(Color.Black);
            foreach (BaseObject obj in _objs)
            {
                obj.Draw();
            }
            Buffer.Render();
        }

        public static void Update()
        {
            foreach (BaseObject obj in _objs)
            {
                obj.Update();
            }
        }

        public static BaseObject[] _objs;
        public static void Load()
        {
            int aCount = 30;
            int a, a1;
            int b, b1;
            int c, c1;

            a = 0; a1 = aCount / 3;
            b = a1; b1 = b + a1;
            c = b + a1; c1 = aCount;

            _objs = new BaseObject[aCount];
            for (int i = a; i < a1; i++)
                _objs[i] = new BaseObject(new Point(600, i * 20), new Point(-i - 2, i + 4), new Size(10, 10));

            for (int i = b; i < b1; i++)
                _objs[i] = new Star(new Point(600, i * 20), new Point(-i, 0), new Size(5, 5));

            int r1 = r.Next(1, 3);
            int r2 = r.Next(1, 4);

            for (int i = c; i < c1; i++)
                _objs[i] = new Square(new Point(300, i * 15), new Point(r1+i, r2+i), new Size(11, 11));
        }
    }
}
