﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyGame
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            Form1 form = new Form1();
            form.Width = 800;
            form.Height = 600;
            SplashScreen.Init(form);
            form.Show();
            SplashScreen.Draw();
            Application.Run(form);



            //Form form = new Form();
            //form.Width = 800;
            //form.Height = 600;
            //SplashScreen.Init(form);
            //form.Show();
            //SplashScreen.Draw();
            //Application.Run(form);

        }
    }
}
