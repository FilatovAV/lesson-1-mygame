﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    class Square : BaseObject
    {
        public Square(Point pos, Point dir, Size size) : base(pos, dir, size)
        {
        }
        public override void Draw()
        {
            SplashScreen.Buffer.Graphics.DrawLine(Pens.White, Pos.X, Pos.Y, Pos.X, Pos.Y + Size.Height);
            SplashScreen.Buffer.Graphics.DrawLine(Pens.White, Pos.X, Pos.Y + Size.Height, Pos.X + Size.Height, Pos.Y + Size.Height);
            SplashScreen.Buffer.Graphics.DrawLine(Pens.White, Pos.X + Size.Height, Pos.Y + Size.Height, Pos.X + Size.Height, Pos.Y);
            SplashScreen.Buffer.Graphics.DrawLine(Pens.White, Pos.X + Size.Height, Pos.Y, Pos.X, Pos.Y);
        }

        public override void Update()
        {
            //Порядок движения

            Pos.X = Pos.X - Dir.X;
            //ir=r.Next(1, 6);
            Pos.Y = Pos.Y - Dir.Y;
            if (Pos.X >= SplashScreen.Width -40) { Dir.X = -Dir.X; }
            if (Pos.X <= 1) { Dir.X = -Dir.X; }

            if (Pos.Y >= SplashScreen.Height - 40) { Dir.Y = -Dir.Y; }
            if (Pos.Y <= 1) { Dir.Y = -Dir.Y; }
        }
    }
}
