﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MyGame
{
    class BaseObject
    {

        protected Point Pos;
        protected Point Dir;
        protected Size Size;
        public BaseObject(Point pos, Point dir, Size size)
        {
            Pos = pos;
            Dir = dir;
            Size = size;
        }
        public virtual void Draw()
        {
            //Game.Buffer.Graphics.DrawEllipse(Pens.White, Pos.X, Pos.Y, Size.Width, Size.Height);
            //Bitmap image = (Bitmap)Image.FromFile(@".\Telegram.png");

            SplashScreen.Buffer.Graphics.DrawImage(Properties.Resources.Telegram, Pos.X, Pos.Y);

            Font drawFont = new Font("Arial", 13);
            SolidBrush drawBrush = new SolidBrush(Color.BlueViolet);
            float x = 150.0F;
            float y = 150.0F;
            SplashScreen.Buffer.Graphics.DrawString("Автор: FilatovAV", drawFont, drawBrush,x, y);
        }

        public virtual void Update()
        {
            Pos.X = Pos.X + Dir.X;
            Pos.Y = Pos.Y + Dir.Y;
            if (Dir.Y==0) { Dir.Y = 1; } //
            if (Pos.X<=0) { Dir.X = -Dir.X; }
            if (Pos.X>= SplashScreen.Width - 100) { Dir.X = -Dir.X; }
            if (Pos.Y<=0) { Dir.Y = -Dir.Y; }
            if (Pos.Y>= SplashScreen.Height - 120) { Dir.Y = -Dir.Y; }
        }
    }
}
